Ontology - Vitis
================


Motivation
----------

As explained by [Krajewski et al (2015)](http://dx.doi.org/10.1093/jxb/erv271), "the availability of good ontologies is a necessary condition for interoperability and semantic links".
More broadly, it is a prerequisite to properly account for confounding factors when jointly modelling and analyzing multiple, heterogeneous data sets potentially acquired by various research teams.


Overview
--------

The released version of the Vitis ontology is hosted by the [CropOntology](http://www.cropontology.org/ontology/VITIS/Vitis) web site.
However, with the increasing number of phenotyped traits and phenotyping methods, it is necessary to update this ontology every once in a while.
Moreover, an ontology being a "shared vocabulary", it is also necessary that this continual process be open and traceable.
Therefore, **this directory contains the development version of the Vitis ontology**, which is versioned using [git](https://en.wikipedia.org/wiki/Git_(software)), and hosted by [MIA's GitLab](https://forgemia.inra.fr/urgi-is/ontologies) web site with "read access" to anyone.


License
-------

The Vitis ontology currently is under the Creative Commons Attribution-ShareAlike 4.0 International license, shortened as [CC BY-SA](http://creativecommons.org/licenses/by-sa/4.0/).
See the file `LICENSE` for more details.


Contribution
------------

The scientific coordinator of the Vitis ontology is [Eric Duchêne](https://www.researchgate.net/profile/Eric_Duchene) (Inra, Colmar).
The informatics coordinator is [Cyril Pommier](https://urgi.versailles.inra.fr/About-us/Team/Information-System-Data-integration/Cyril-Pommier) (Inra, Versailles).

If you are interested and wish to know more, or even to contribute, don't hesitate to contact one or both of them!
